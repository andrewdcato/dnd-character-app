//
//  AppDelegate.swift
//  DnD Character Sheet
//
//  Created by Andrew Cato on 6/10/15.
//  Copyright (c) 2015 Andrew Cato. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

